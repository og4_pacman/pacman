package gui;

import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;


public class Startbildschirm extends JFrame
{
	private JButton btnStart;
	private JButton btnHighscore;
	private JButton btnBeenden;
	
	
	public Startbildschirm()			// GUI mit dem Titel Option wird erstellt
	{
	setTitle("Option");				
	setSize(600,600);
	setResizable(false);
	setLayout(null);
	setVisible(true);	 
	
	// Auf der GUI werden drei Buttons projiziert																												
	
	btnStart = new JButton("Start ");
	btnStart.setBounds(125,275,150,50);
	add(btnStart); 
	
	btnHighscore = new JButton("Highscore ");
	btnHighscore.setBounds(350,275,150,50);
	add(btnHighscore);	
	
	btnBeenden = new JButton("Beenden");
	btnBeenden.setBounds(250,500,150,50);
	add(btnBeenden);
	
	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	
	
	
		btnStart.addActionListener (new ActionListener()     			
		 	{
		 		
	 		public void actionPerformed(ActionEvent e)		// durch den Button Start wird das Spiel gestartet
			 {
				new  Spielfeld();
				dispose();
				
			}
				
			});
	 	
		btnHighscore.addActionListener (new ActionListener()  // durch den Highscore Button wird die Datenbank mit der highscore liste aufgerufen
	 	{
	 		public void actionPerformed(ActionEvent e)		 
			 {
					
					dispose();
			}
	 	});
	 	
		btnBeenden.addActionListener (new ActionListener()		// durch den Beenden Button wird die GUI geschlossen
	 	{
	 		public void actionPerformed(ActionEvent e)		 
			 {
	 			System.exit(0);
	 			dispose();
			}
	 	});
	}
	
	// hier wird ein Bild auf der GUI eingefügt
	public void paint (Graphics g)
	{
		super.paint(g);
		g.drawImage(Toolkit.getDefaultToolkit().getImage("images/Firmenlogo.png"),110,50,this);
	}
	
	 	
}
