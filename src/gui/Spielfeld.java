package gui;

import ch.aplu.jgamegrid.*;

public class Spielfeld extends GameGrid
{
	private Pacman pacman = new Pacman();
	private Geist1 geist1 = new Geist1(pacman);
	private Geist2 geist2 = new Geist2(pacman);
	private Geist3 geist3 = new Geist3(pacman);
	
	private Busch busch1 = new Busch();
	private Busch busch2 = new Busch();
	private Busch busch3 = new Busch();
	private Busch busch4 = new Busch();
	private Busch busch5 = new Busch();
	private Busch busch6 = new Busch();
	private Busch busch7 = new Busch();
	private Busch busch8 = new Busch();
	private Busch busch9 = new Busch();
	private Busch busch10 = new Busch();
	private Busch busch11 = new Busch();
	private Busch busch12 = new Busch();
	private Busch busch13 = new Busch();
	private Busch busch14 = new Busch();
	private Busch busch15 = new Busch();
	private Busch busch16 = new Busch();
	private Busch busch17 = new Busch();
	private Busch busch18 = new Busch();
	private Busch busch19 = new Busch();
	private Busch busch20 = new Busch();
	private Busch busch21 = new Busch();
	private Busch busch22 = new Busch();
	private Busch busch23 = new Busch();
	private Busch busch24 = new Busch();
	private Busch busch25 = new Busch();
	private Busch busch26 = new Busch();
	private Busch busch27 = new Busch();
	private Busch busch28 = new Busch();
	private Busch busch29 = new Busch();
	private Busch busch30 = new Busch();
	private Busch busch31 = new Busch();
	private Busch busch32 = new Busch();
	private Busch busch33 = new Busch();
	private Busch busch34 = new Busch();
	private Busch busch35 = new Busch();
	private Busch busch36 = new Busch();
	private Busch busch37 = new Busch();
	private Busch busch38 = new Busch();
	private Busch busch39 = new Busch();
	private Busch busch40 = new Busch();
	private Busch busch41 = new Busch();
	private Busch busch42 = new Busch();
	private Busch busch43 = new Busch();
	private Busch busch44 = new Busch();
	private Busch busch45 = new Busch();
	private Busch busch46 = new Busch();
	private Busch busch47 = new Busch();
	private Busch busch48 = new Busch();
	private Busch busch49 = new Busch();
	private Busch busch50 = new Busch();
	private Busch busch51 = new Busch();
	private Busch busch52 = new Busch();
	private Busch busch53 = new Busch();
	private Busch busch54 = new Busch();
	private Busch busch55 = new Busch();
	private Busch busch56 = new Busch();
	private Busch busch57 = new Busch();
	private Busch busch58 = new Busch();
	private Busch busch59 = new Busch();
	private Busch busch60 = new Busch();
	private Busch busch61 = new Busch();
	
	
	

	
	public Spielfeld()
	{				// es wird ein Gitternetz erzeugt mit den Ma�en  16*14 und Feldgr��e von 70 Pixel
						
		super(16,14,70,null, "images/background.PNG", false); // 
		show();
	
		playLoop("sound/_pacman.wav");
		
		 	setTitle("Plantman");
			
		 	addActor(pacman, new Location(2, 7));
			addKeyListener(pacman);
			
			addActor(busch1,new Location(1,6));
			addActor(busch2,new Location(1,10));
			addActor(busch3,new Location(2,3));
			addActor(busch4,new Location(2,4));
			addActor(busch5,new Location(2,6));
			addActor(busch6,new Location(2,8));
			addActor(busch7,new Location(2,10));
			addActor(busch8,new Location(2,12));
			addActor(busch9,new Location(3,3));
			addActor(busch58,new Location(3,6));
			addActor(busch10,new Location(3,8));
			addActor(busch11,new Location(3,12));
			addActor(busch12,new Location(4,5));
			addActor(busch13,new Location(4,6));
			addActor(busch14,new Location(4,8));
			addActor(busch15,new Location(4,9));
			addActor(busch16,new Location(4,10));
			addActor(busch17,new Location(4,12));
			addActor(busch18,new Location(5,3));
			addActor(busch19,new Location(5,6));
			addActor(busch20,new Location(6,3));
			addActor(busch21,new Location(6,4));
			addActor(busch22,new Location(6,8));
			addActor(busch23,new Location(6,10));
			addActor(busch24,new Location(6,11));
			addActor(busch25,new Location(6,12));
			addActor(busch26,new Location(7,4));
			addActor(busch27,new Location(7,6));
			addActor(busch28,new Location(7,10));
			addActor(busch29,new Location(7,11));
			addActor(busch30,new Location(8,3));
			addActor(busch31,new Location(8,4));
			addActor(busch32,new Location(8,6));
			addActor(busch33,new Location(8,7));
			addActor(busch34,new Location(8,8));
			addActor(busch35,new Location(8,10));
			addActor(busch36,new Location(8,11));
			addActor(busch37,new Location(8,12));
			addActor(busch38,new Location(9,3));
			addActor(busch39,new Location(9,4));
			addActor(busch40,new Location(9,12));
			addActor(busch41,new Location(10,6));
			addActor(busch42,new Location(10,8));
			addActor(busch43,new Location(10,9));
			addActor(busch44,new Location(10,10));
			addActor(busch45,new Location(10,12));
			addActor(busch46,new Location(11,3));
			addActor(busch47,new Location(11,5));
			addActor(busch48,new Location(11,6));
			addActor(busch49,new Location(11,8));
			addActor(busch50,new Location(12,3));
			addActor(busch51,new Location(12,10));
			addActor(busch52,new Location(12,11));
			addActor(busch53,new Location(12,12));
			addActor(busch54,new Location(13,5));
			addActor(busch55,new Location(13,6));
			addActor(busch56,new Location(13,8));
			addActor(busch57,new Location(13,9));
			addActor(busch59,new Location(14,3));
			addActor(busch60,new Location(14,11));
			addActor(busch61,new Location(14,12));
			
		
	
			addActor(geist1,new Location(2,5));		//Easy
		   	geist1.setSlowDown(1);
		    //addActor(geist2,new Location(9,14));		//Medium
		    //addActor(geist3,new Location(15,14));		//Hard
			
		    for(int i = 0; i <9; i++)
			addActor(new Cherry(), getRandomEmptyLocation());
		   	
			for(int i = 0; i <5; i++)
			addActor(new Cherry2(), getRandomEmptyLocation());
			
		   	for(int i = 0; i <100; i++)
		   	addActor(new Food(),getRandomEmptyLocation());
		   	
		   	for(int i = 0; i <1; i++)
			addActor(new Taco(),getRandomEmptyLocation());
		   	
		   	
		   	
		   	show();
		   	doRun();
			
	}
		
	
	
}
