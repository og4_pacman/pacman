package gui;

import ch.aplu.jgamegrid.Actor;

public class Geist2 extends Actor
{
	private Pacman player;
	
	public Geist2(Pacman player) 
	{
		super(true,"images/ghost.png");
		this.player = player;

	}
	public void act()
	  {
		setHorzMirror(false);
        gameGrid.setPaintOrder(Geist2.class);
		
	    if (nbCycles % 5 == 0 && !player.isRemoved())
	    {
	      setDirection(getLocation().getCompassDirectionTo(player.getLocation()));
	      move();
	    }
	    Actor player = gameGrid.getOneActorAt(getLocation(), Pacman.class);
	    if (player != null){
	     player.removeSelf();
	    System.out.println("Game Over!");
	    }
	  }
}
