package gui;

import ch.aplu.jgamegrid.Actor;
import ch.aplu.jgamegrid.Location;
import ch.aplu.jgamegrid.Location.CompassDirection;

public class Geist1 extends Actor {
	private Pacman player;

	public Geist1(Pacman player) {
		super(true, "images/ghost.png");
		this.player = player;

	}

	public void act() {
		setHorzMirror(false);
		gameGrid.setPaintOrder(Geist1.class);

		// hier ist der teil der verhindert , dass der geist durch die wand geht
		if (nbCycles % 5 == 0 && !player.isRemoved()) {
			// setzt die Direction auf die Location des Players , hinzu kommt,
			// dass festgelegt wird das sich nur in 4 Richtungen bewegt werden
			// kann
			CompassDirection gotoPacman = getLocation().get4CompassDirectionTo(player.getLocation());
			setDirection(gotoPacman);
			
			CompassDirection gostraigt = getLocation().getCompassDirectionTo(player.getLocation());

			Location l = this.getNextMoveLocation();
			Actor actor = gameGrid.getOneActorAt(l, Busch.class);
			if (actor == null && isMoveValid()) {
				move();
			}
			else 
			{
				if(gotoPacman == Location.SOUTH)
				{
					if(gostraigt == Location.SOUTHWEST)
					{
						setDirection(Location.WEST);
					} else {
						 setDirection(Location.EAST);
					}
				}
			}
		}

		Actor player = gameGrid.getOneActorAt(getLocation(), Pacman.class);
		if (player != null) {
			player.removeSelf();
			System.out.println("Game Over!");

		}
	}
}
