package gui;

import java.awt.event.KeyEvent;
import ch.aplu.jgamegrid.Actor;
import ch.aplu.jgamegrid.GGKeyListener;
import ch.aplu.jgamegrid.Location;


public class Pacman extends Actor implements GGKeyListener {

	int score = 0;

	public Pacman() {
		super(true, "images/plantman.png");

	}

	@Override
	public boolean keyPressed(KeyEvent evt) 			// normal Richtung von Pacman ist Rechts; also 0 Grad  
														// nach unten ist 90 Grad ; nach links 180; und nach oben 270
	{
		switch (evt.getKeyCode()) {
		case KeyEvent.VK_UP:
			setDirection(270);
			break;
		case KeyEvent.VK_RIGHT:
			setDirection(0);
			break;
		case KeyEvent.VK_LEFT:
			setDirection(180);
			break;
		case KeyEvent.VK_DOWN:
			setDirection(90);
			break;
		}		
		

		return true;
	}

	@Override
	public boolean keyReleased(KeyEvent evt) {
		return true;
	}
	
	public void act(){		
		
		Location l = this.getNextMoveLocation();
		Actor actor = gameGrid.getOneActorAt(l, Busch.class);
		if(actor == null && isMoveValid()){			
			move();		
			tryToEat();
		}
		
		//1.Schritt. Auslesen der Zelle auf die sich PacMan bewegen m�chte
		//2.Schritt. �berpr�fen ob die Zelle eine Wand ist
		//Wenn Wand, dann nix
		//Sonst bewege
		
		
	}
		
	public void tryToEat() {
		Actor actor = gameGrid.getOneActorAt(getLocation(), Cherry.class);				// �berp�fen ob sich Futter auf einem Feld befindet
																						// wenn ja wird der Actor aus dem Gitter eliminiert
																						// und es werden je nach Wert des Futters Punkte auf den Score addiert
		
		if (actor != null) {
			actor.removeSelf();
			this.score = score + 75;
		}
		Actor actor3 = gameGrid.getOneActorAt(getLocation(), Cherry2.class);
		if (actor3 != null) {
			actor3.removeSelf();
			this.score = score + 125;
		}

		Actor actor2 = gameGrid.getOneActorAt(getLocation(), Food.class);
		if (actor2 != null) {
			actor2.removeSelf();
			this.score = score + 10;
		}
		
		Actor actor4 = gameGrid.getOneActorAt(getLocation(), Taco.class);
		if (actor4 != null) {
			actor4.removeSelf();
			this.score = score + 500;
		}
		

		System.out.println("score : " + score);

	}
	
	
}